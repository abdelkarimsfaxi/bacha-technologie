/*eslint-disable*/
import React from "react";

import IndexNavbar from "components/Navbars/IndexNavbar.js";
import Footer from "components/Footers/Footer.js";
import Logo from "../assets/img/logo.jpg";
import KIWI from "../assets/img/kiwi.png";

export default function Index() {
  return (
    <>
      <IndexNavbar fixed />
      <section
        id="about"
        className="header relative pt-16 items-center flex h-screen max-h-860-px"
      >
        <div className="container mx-auto items-center flex flex-wrap">
          <div className="w-full md:w-8/12 lg:w-6/12 xl:w-6/12 px-4">
            <div className="pt-24 sm:pt-0">
              <img src={Logo} className="h-24 w-auto" />
              <p className="mt-4 ml-4 text-lg leading-relaxed text-gray-600">
              Specialized in recruiting Tunisian engineers on behalf of more than 10 renowned Canadian IT services companies.<br/>
                We are a team of dedicated software developers, creative
                designers, digital marketing enthusiasts, and strategists
                working collaboratively to foster a dynamic and supportive work
                environment.
              </p>
            </div>
          </div>
        </div>

        <img
          className="absolute top-0 b-auto right-0 pt-16 sm:w-6/12 -mt-48 sm:mt-0 w-10/12 max-h-860px"
          src={require("assets/img/pattern_react.png")}
          alt="..."
        />
      </section>
      <div id="services"></div>
      <section className="mt-48 md:mt-40 pb-40 relative bg-gray-200">
        <div
          className="-mt-20 top-0 bottom-auto left-0 right-0 w-full absolute h-20"
          style={{ transform: "translateZ(0)" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-200 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
        <div className="container mx-auto">
          <div className="flex flex-wrap items-center">
            <div className="w-10/12 md:w-6/12 lg:w-4/12 px-12 md:px-4 mr-auto ml-auto -mt-32">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-blue-600">
                <img
                  alt="..."
                  src="https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80"
                  className="w-full align-middle rounded-t-lg"
                />
                <blockquote className="relative p-8 mb-4">
                  <svg
                    preserveAspectRatio="none"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 583 95"
                    className="absolute left-0 w-full block h-95-px -top-94-px"
                  >
                    <polygon
                      points="-30,95 583,95 583,65"
                      className="text-blue-600 fill-current"
                    ></polygon>
                  </svg>
                  <h4 className="text-xl font-bold text-white">
                    End-to-end expertise
                  </h4>
                  <p className="text-md font-light mt-2 text-white">
                    We handle all aspects of your project at every stage of the
                    development process in an agile way. From the initial idea
                    stage through project implementation, we are invested in
                    your business success.
                  </p>
                </blockquote>
              </div>
            </div>

            <div className="w-full md:w-6/12 px-4">
              <div className="flex flex-wrap">
                <div className="w-full md:w-6/12 px-4">
                  <div className="relative flex flex-col mt-4">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-white">
                        <i className="fas fa-newspaper"></i>
                      </div>
                      <h6 className="text-xl mb-1 font-semibold">
                        Web development
                      </h6>
                      <p className="mb-4 text-gray-600">
                        We deliver a wide range of custom, end-to-end web
                        development services that cater to various business
                        needs and industries.
                      </p>
                    </div>
                  </div>
                  <div className="relative flex flex-col min-w-0">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-white">
                        <i className="fas fa-sitemap"></i>
                      </div>
                      <h6 className="text-xl mb-1 font-semibold">
                        Web Hosting
                      </h6>
                      <p className="mb-4 text-gray-600">
                        We help companies leverage Cloud capabilities and
                        solutions to scale and transform their business
                        processes.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="w-full md:w-6/12 px-4">
                  <div className="relative flex flex-col min-w-0 mt-4">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-white">
                        <i className="fas fa-drafting-compass"></i>
                      </div>
                      <h6 className="text-xl mb-1 font-semibold">
                        UX / UI Desgin
                      </h6>
                      <p className="mb-4 text-gray-600">
                        We provide intuitive User Interface (UI) and engaging
                        User Experience (UX) design services for web and mobile
                        applications across various industries.
                      </p>
                    </div>
                  </div>
                  <div className="relative flex flex-col min-w-0">
                    <div className="px-4 py-5 flex-auto">
                      <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-white">
                        <i className="fas fa-file-alt"></i>
                      </div>
                      <h6 className="text-xl mb-1 font-semibold">
                        Digital Marketing
                      </h6>
                      <p className="mb-4 text-gray-600">
                        Our Digital Marketing team and SEO consultants will help
                        you craft an effective digital marketing strategy that
                        will set you apart from the competition.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container mx-auto px-4 pb-32 pt-48">
          <div className="items-center flex flex-wrap">
            <div className="w-full md:w-5/12 ml-auto px-12 md:px-4">
              <div className="md:pr-12">
                <div className="text-gray-600 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-white">
                  <i className="fas fa-file-alt text-xl"></i>
                </div>
                <h3 className="text-3xl font-semibold">Web Service</h3>
                <p className="mt-4 text-lg leading-relaxed text-gray-600">
                  We develop websites that allow you to reach your users where
                  that they operate while leaving a memorable impression on
                  them. Expert, creative and insightful, our approach keeps your
                  key goals in mind at all times to provide a platform that
                  benefits from a unique user experience. Creation of a website
                  via WordPress with a template of your choice and according to
                  your needs which follows the following steps
                </p>
              </div>
            </div>

            <div className="w-full md:w-6/12 mr-auto px-4 pt-24 md:pt-0">
              <img
                alt="..."
                className="max-w-full rounded-lg shadow-xl"
                style={{
                  transform:
                    "scale(1) perspective(1040px) rotateY(-11deg) rotateX(2deg) rotate(2deg)"
                }}
                src={require("assets/img/documentation.png")}
              />
            </div>
          </div>
        </div>
        <div id="clients"></div>
        <div className="justify-center text-center flex flex-wrap mt-24">
          <div className="w-full md:w-6/12 px-12 md:px-4">
            <h2 className="font-semibold text-4xl">Our Clients</h2>
            <div className="flex  font-semibold text-4xl text-center justify-center items-center">
              <img className="h-20 w-auto" src={KIWI} />
            </div>
            <p className="text-lg leading-relaxed mt-4 mb-4 text-gray-600">
              For nearly 20 years,{" "}
              <a className="text-blue-500" href="https://www.kiwi.ca/">
                KIWI Technologies
              </a>{" "}
              has helped simplify the daily lives of teams using its weHoop
              workflow platform, which promotes collaboration and facilitates
              the breaking down of business silos.
            </p>
          </div>
        </div>
      </section>

      <section id="contacts" className="pb-16 bg-gray-300 relative">
        <div
          className="-mt-20 top-0 bottom-auto left-0 right-0 w-full absolute h-20"
          style={{ transform: "translateZ(0)" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-300 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </section>
      <Footer />
    </>
  );
}
