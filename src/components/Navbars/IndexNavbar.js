/*eslint-disable*/
import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/img/logo_website.png";


export default function Navbar(props) {
  const [navbarOpen, setNavbarOpen] = React.useState(false);
  return (
    <>
      <nav className="top-0 fixed z-50 w-full flex flex-wrap items-center justify-between px-2 py-1 navbar-expand-lg bg-white shadow">
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              to="/"
              className="text-gray-800 text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap uppercase"
            >
              <img src={Logo} className="h-10 w-auto" alt="" />
            </Link>
          </div>
          <div
            className={
              "lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none" +
              (navbarOpen ? " block" : " hidden")
            }
            id="example-navbar-warning"
          >
            <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
              <li>
                <a
                  className="hover:text-blue-600 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="#about"
                >
                  About
                </a>
              </li>
              <li>
                <a
                  className="hover:text-blue-600 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="#services"
                >
                  Services
                </a>
              </li>
              <li>
                <a
                  className="hover:text-blue-600 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="#clients"
                >
                  Clients
                </a>
              </li>
              <li>
                <a
                  className="hover:text-blue-600 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="#contacts"
                >
                  Contacts
                </a>
              </li>
              <li className="flex items-center">
                <a
                  className="hover:text-blue-500 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="https://www.facebook.com/BACHA-Technologie-100301201324600/"
                  target="_blank"
                >
                  <i className="text-gray-500 hover:text-blue-500 fab fa-facebook text-lg leading-lg " />
                </a>
              </li>

              <li className="flex items-center">
                <a
                  className="hover:text-blue-500 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                  href="https://www.linkedin.com/company/bacha-technologie/"
                  target="_blank"
                >
                  <i className="text-gray-500 hover:text-blue-500 fab fa-linkedin text-lg leading-lg " />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
