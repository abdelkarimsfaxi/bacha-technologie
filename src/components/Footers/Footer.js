import React from "react";

export default function Footer() {
  return (
    <>
      <footer className="relative bg-gray-300 pt-8 pb-6">
        <div
          className="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20 h-20"
          style={{ transform: "translateZ(0)" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-gray-300 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
        <div className="container mx-auto px-4">
          <div className="flex flex-wrap text-center lg:text-left">
            <div className="w-full lg:w-6/12 px-4">
              <h4 className="text-3xl font-semibold">Social Network</h4>
              <div className="mt-6 lg:mb-0 mb-6">
                <button
                  className="bg-white text-blue-400 hover:text-white hover:bg-blue-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
                  type="button"
                >
                  <i className="fab fa-twitter"></i>
                </button>
                <button
                  className="bg-white text-blue-600 hover:text-white hover:bg-blue-600 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
                  type="button"
                >
                  <a href="https://www.facebook.com/BACHA-Technologie-100301201324600/">
                    <i className="fab fa-facebook-square"></i>
                  </a>
                </button>
                <button
                  className="bg-white text-blue-400 hover:text-white hover:bg-blue-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
                  type="button"
                >
                  <a href="https://www.linkedin.com/company/bacha-technologie/">
                    <i className="fab fa-linkedin"></i>
                  </a>
                </button>
              </div>
            </div>
            <div className="w-full lg:w-6/12 px-4">
              <div className="flex flex-wrap items-top mb-6">
                <div className="w-full lg:w-5/12 px-4"></div>
                <div className="w-full lg:w-7/12">
                  <span className="block uppercase text-gray-600 text-sm font-semibold mb-2">
                    Contact Us
                  </span>
                  <ul className="list-unstyled">
                    <li>
                      <i className="fas ml-1/2 fa-map-marker-alt"></i> &nbsp; &nbsp; 000,
                      RTE KM2 Gabes Medinine, Tunisie
                    </li>
                    <li>
                      <i className="far fa-envelope"></i> &nbsp;
                      &nbsp;responsable-commercial@bachatec.com
                    </li>
                    <li>
                      <i className="ml-1/2 fas fa-mobile-alt"></i>&nbsp; &nbsp; &nbsp;+216
                      55 504 219
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <hr className="my-6 border-gray-400" />
          <div className="flex flex-wrap items-center md:justify-between justify-center">
            <div className="w-full md:w-4/12 px-4 mx-auto text-center">
              <div className="text-sm text-gray-600 font-semibold py-1">
                Copyright © 2021 by Bacha Technologie.
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
